SRC_URI += "file://imx8mq-maaxboard.dts;subdir=git/arch/arm64/boot/dts/freescale \
            "

FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

S = "${WORKDIR}/git"

COMPATIBLE_MACHINE = "(mx8)"

# We need to pass it as param since kernel might support more then one
# machine, with different entry points
KERNEL_EXTRA_ARGS += "LOADADDR=${UBOOT_ENTRYPOINT}"




FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

LICENSE = "CLOSED"
BB_STRICT_CHECKSUM = "0"

SRC_URI = "file://u-boot_2018.03._develop_50b1e8d144.tar.gz"
S = "${WORKDIR}/u-boot"

SCMVERSION = ""


do_deploy_append_mx8m() {
	rm -rf ${DEPLOYDIR}/${BOOT_TOOLS}/mkimage_uboot
}
